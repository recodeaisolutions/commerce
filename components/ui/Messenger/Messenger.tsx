import React from 'react';
 // @ts-ignore
import MessengerCustomerChat from "react-messenger-customer-chat";

const Messenger = () => {
    return(
        <div>
            <MessengerCustomerChat
                pageId="347728615910555"
                appId="680214799110372"
                className="fb-customerchat"
                attribution="setup_tool"
                themeColor="#007bff"
                loggedInGreeting="Welcome to Home Grown"
                loggedOutGreeting="Welcome to Home Grown"
                shouldShowDialog={true}
            />
        </div>
    );
}

export default Messenger;
